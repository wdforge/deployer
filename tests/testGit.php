<?php

include __DIR__ . "/../vendor/autoload.php";

\Deployment\Command\Git::loadConfig(__DIR__ . '/../projects/vitrina/config.php');

$newPath = '/tmp/newDirectory'.rand(0,1000);
$url = 'git@gitlab.com:wdforge/engine.git';
$hashCommit = "034c956387c79c713dd66d122cfc3c0a9f09b327";

$res = \Deployment\Command\Directory::create($newPath);
echo sprintf("\nDirectory %s - created - result:%d\n", $newPath, $res);

$clonedDirectory = \Deployment\Command\Git::clone($url, $newPath);
echo sprintf("\nProject cloned %s \n", $clonedDirectory);

$result = \Deployment\Command\Git::setCurrentCommit($clonedDirectory, $hashCommit, false);
echo sprintf("\nProject set current commit %d \n", $result);

$result = \Deployment\Command\Git::getCurrentCommit($clonedDirectory);

echo sprintf("\nCurrent commit %s \n", $result);
