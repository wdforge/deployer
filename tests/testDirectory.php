<?php
include __DIR__ . "/../vendor/autoload.php";

$newPath = '/tmp/newDirectory'.rand(0,1000);

use \Deployment\Command\Directory;

try {
    $res = Directory::create($newPath);
    echo sprintf("Directory %s - created - result:%d\n", $newPath, $res);

    \Deployment\Command\Symlink::addSymlink('/home/xxx/deploy/static/media', $newPath.'/media');

    Directory::delete($newPath);
    echo sprintf("Directory %s - deleted\n", $newPath);

    Directory::create($newPath);
    echo sprintf("Directory %s - created\n", $newPath);
    \Deployment\Command\Symlink::addSymlink('/home/xxx/deploy/static/media', $newPath.'/media');

    Directory::setChmod($newPath, 0775);
    echo sprintf("Directory %s - set rules\n", $newPath);

    Directory::move($newPath, $newPath.'_new');

    Directory::delete($newPath.'_new');
    echo sprintf("Directory %s - deleted\n", $newPath.'_new');
}
catch(Exception $e) {
    throw $e;
}