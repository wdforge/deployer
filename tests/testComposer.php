<?php
include __DIR__ . "/../vendor/autoload.php";

$newPath = '/tmp/newDirectory'.rand(0,1000);
$url = 'git@gitlab.com:wdforge/ScnSocialAuth.git';
$res = \Deployment\Directory::create($newPath);
echo sprintf("Directory %s - created - result:%d\n", $newPath, $res);

$projectDirectory = \Deployment\Git::clone($url, $newPath);
\Deployment\Directory::setCurrentDirectory($projectDirectory);
$result = \Deployment\Composer::update();

echo sprintf("Composer update - result:%d\n", $result);






