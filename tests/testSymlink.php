<?php
include __DIR__ . "/../vendor/autoload.php";

$newPath = '/tmp/newDirectory'.rand(0,1000);
$newSymlinkPath = '/tmp/newDirectory'.rand(0,1000);

$res = \Deployment\Directory::create($newPath);
echo sprintf("Directory %s - created - result:%d\n", $newPath, $res);

Deployment\Symlink::addSymlink($newPath, $newSymlinkPath);

$result = Deployment\Symlink::isSymlink($newSymlinkPath);
echo sprintf("Symlink %s - created - result:%d\n", $newSymlinkPath, $result);

$result = Deployment\Symlink::removeSymlink($newSymlinkPath);
echo sprintf("Symlink %s - deleted - result:%d\n", $newSymlinkPath, $result);

$res = \Deployment\Directory::delete($newPath);
echo sprintf("Directory %s - deleted - result:%d\n", $newPath, $res);