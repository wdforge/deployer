<?php

namespace Deployment;

use Deployment\Facade\Core\Logger;

/**
 * Class ErrorHandler
 * @package Deployment\Core
 */
class ErrorHandler
{
    /**
     * Формирование записи ошибки в лог.
     *
     * @param integer $errno номер ошибки
     * @param string $errstr сообщение об ошибке
     * @param string $errfile фаил
     * @param string $errline
     *
     * @return true | none
     */
    public function stringifyErrorType($type)
    {
        if (is_object($type)) {
            return 'exception';
        }
        switch ($type) {
            case E_ERROR: // 1 //
                return 'FATAL';
            case E_WARNING: // 2 //
                return 'E_WARNING';
            case E_PARSE: // 4 //
                return 'E_PARSE';
            case E_NOTICE: // 8 //
                return 'E_NOTICE';
            case E_CORE_ERROR: // 16 //
                return 'E_CORE_ERROR';
            case E_CORE_WARNING: // 32 //
                return 'E_CORE_WARNING';
            case E_COMPILE_ERROR: // 64 //
                return 'E_COMPILE_ERROR';
            case E_COMPILE_WARNING: // 128 //
                return 'E_COMPILE_WARNING';
            case E_USER_ERROR: // 256 //
                return 'E_USER_ERROR';
            case E_USER_WARNING: // 512 //
                return 'E_USER_WARNING';
            case E_USER_NOTICE: // 1024 //
                return 'E_USER_NOTICE';
            case E_STRICT: // 2048 //
                return 'E_STRICT';
            case E_RECOVERABLE_ERROR: // 4096 //
                return 'E_RECOVERABLE_ERROR';
            case E_DEPRECATED: // 8192 //
                return 'E_DEPRECATED';
            case E_USER_DEPRECATED: // 16384 //
                return 'E_USER_DEPRECATED';
        }
        return $type;
    }

    /**
     * @param int $errno
     * @param string $errstr
     * @param string $errfile
     * @param string $errline
     * @return bool
     * @throws \Deployment\Exception\FileIsNotWriteable
     * @throws \Deployment\Exception\NotFoundFile
     */
    public function errorHandle($errno = 0, $errstr = '', $errfile = '', $errline = '')
    {
        $message = "";
        $criticalError = false;
        if (!is_object($errno)) {
            switch ($errno) {
                case E_ERROR:
                case E_COMPILE_ERROR:
                case E_CORE_ERROR:
                case E_PARSE:
                case E_USER_ERROR:
                case E_WARNING:
                    if ($errstr == "(SQL)") {
                        // handling an sql error
                        $message .= "<b>SQL Error</b> [$errno] " . SQLMESSAGE . "<br />\n";
                        $message .= "Query : " . SQLQUERY . "<br />\n";
                        $message .= "On line " . SQLERRORLINE . " in file " . SQLERRORFILE . " ";
                        $message .= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                        $message .= "Aborting...<br />\n";
                    } else {
                        $message .= "<b>" . $this->stringifyErrorType($errno) . "</b> [$errno] $errstr<br />\n";
                        $message .= ($errno == E_WARNING ? ' Warning' : ' Fatal error') . "  on line $errline in file $errfile";
                        $message .= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                        $message .= "Aborting...<br />\n";
                    }

                    $criticalError = true;
                    break;
                default :
                    {
                        $message .= "<b>" . $this->stringifyErrorType($errno) . "</b> [$errno] $errstr<br />\n";
                        $message .= "  warning on line $errline in file $errfile";
                        $message .= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                        break;
                    }
            }
        } else {
            /* @var Exception $errno */
            $message .= "<b> Uncatched Exception </b>  " . $errno->getMessage() . "<br />\n";
            $message .= "  warning on line " . $errno->getLine() . " in file " . $errno->getFile();
            $message .= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            $criticalError = true;
        }

        try {
            if (is_object($errno)) {
                /* @var Exception $errno */
                throw $errno;
            } else {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            $trace = explode("\n", $e->getTraceAsString());
            array_shift($trace);

            $message .= "\n" . implode("<BR>\n", $trace);
        }

        Logger::log($message);

        if ($criticalError) {
            if (ini_get('display_errors')) {
                echo $message;
            } else {
                echo 'error';
            }
            die();
        }

        if (ini_get('display_errors'))
            echo $message;

        return true;
    }

    /**
     * @throws \Deployment\Exception\FileIsNotWriteable
     * @throws \Deployment\Exception\NotFoundFile
     */
    public function shutdownHandler()
    {
        $error = error_get_last();
        if (empty($error) || $error['type'] != E_ERROR) {
            return;
        }
        $this->errorHandle(E_ERROR, $error['message'], $error['file'], $error['line']);
    }

    /**
     * @param $errno
     * @param $errstr
     * @param $errfile
     * @param $errline
     * @param $context
     * @throws \Deployment\Exception\FileIsNotWriteable
     * @throws \Deployment\Exception\NotFoundFile
     */
    public function errorException($errno, $errstr, $errfile, $errline, $context)
    {
        $this->errorHandle($errno, $errstr, $errfile, $errline);
    }

    /**
     *
     */
    public function init()
    {
        set_error_handler([$this, 'errorHandle']);
        set_exception_handler([$this, 'errorHandle']);
        register_shutdown_function([$this, 'shutdownHandler']);
    }
}