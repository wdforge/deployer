<?php

namespace Deployment\Traits;

use Deployment\Exception\LoggedException;

/**
 * Trait Process
 * @package Deployment\Traits
 *
 * @property $procname
 */
trait Process
{
    /**
     * проверка на запуск копии, если есть то выходим.
     */
    public function checkSingle()
    {
        $procname = preg_replace(["/^.*\//", "/\..*$/"], '', $this->procname); //!!!!!!! 100% ошибка!!!
        $pidfile = '/var/run/' . $procname . '.pid';
        $stored_pid = (int)@file_get_contents($pidfile);

        if ($stored_pid != 0) {
            $stored_argv = @explode("\0", @file_get_contents('/proc/' . $stored_pid . '/cmdline'));
            $stored_procname = preg_replace(["/^.*\//", "/\..*$/"], '', @$stored_argv[1]);

            if ($stored_procname == $procname) {
                throw new LoggedException("Запуск нескольких копий невозможен");
                exit;
            }

            @unlink($pidfile);
        }

        //если нет, ставим маркер
        @file_put_contents($pidfile, posix_getpid());
    }

}