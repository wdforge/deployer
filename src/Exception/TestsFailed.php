<?php

namespace Deployment\Exception;


use Throwable;

class TestsFailed extends LoggedException
{
    public function __construct(array $messages, int $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf("Tests failed report\n%s\n---------------------------------\n", var_export($messages, true)), $code, $previous);
    }
}