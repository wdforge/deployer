<?php

namespace Deployment\Deploy;

/**
 * класс в котором определяется общее поведение с деплоями, задается карта событий,
 * вызывается загрузка конфига
 * происходит инициализация процесса,
 */
use Deployment\Exception\FileIsNotReadable;
use Deployment\Traits\Events;
use Deployment\Traits\Process;
use Deployment\Facade\Core\Logger;
use Deployment\Facade\Core\Configuration;
use Deployment\Facade\Command\Directory;
use Deployment\Exception\TestsFailed;

/**
 *
 * Class Deploy
 * @package Deployment
 */

/**
 * @property $isLogging bool
 */
class Deploy extends Configuration implements IDeploy
{
    use Process, Events;

    protected static $_instance;
    protected static $_testsData;

    /**
     * ошибки при деплое
     *
     * @var array
     */
    protected $_errors = [];

    /**
     * инициализация
     */
    public static function init()
    {
        static::getConfig();

        /**
         * запрет повторного запуска
         */
        if (static::getInstance()->singleProcess) {
            static::getInstance()->checkSingle();
        }

        if (static::getInstance()->isHookError) {
            static::getInstance()->errorHanager = new \Deployment\ErrorHandler();
            static::getInstance()->errorHanager->init();
        }

        /**
         * переданные аргументы перегружают параметры конфига
         */
        static::mergeCommands($_SERVER['argv']);

        return static::getInstance();
    }

    /**
     * развертывание последней версии проекта
     * запуск тестирования
     */
    public function upgrade()
    {
        if (static::getConfig('isTestsExecute')) {
            Logger::Log("Running tests!");
            $result = $this->afterTests(static::getConfig('isSilentTestFail', true));
            if ($result) {
                Logger::Log("Result tests: OK");
            } else {
                $data = $this->getTestsData();
                Logger::Log(sprintf("Result tests: FAIL\n%s\n-------------------------\n", $data));
            }
        }

        Logger::Log(sprintf("Deploy::upgrade(%s)", $this->procname));
    }

    /**
     * развертывание предыдущей версии проекта
     */
    public function downgrade()
    {
        Logger::Log(sprintf("Deploy::downgrade(%s)", $this->procname));
    }

    /**
     * развертывание предыдущей версии проекта
     */
    public function setgrade()
    {
        Logger::Log(sprintf("Deploy::setgrade(%s)", $this->procname));
    }

    /**
     *
     * @return \Deployment\Deploy
     */
    public static function getInstance()
    {
        if (!static::$_instance) {
            static::$_instance = new static;
        }
        return static::$_instance;
    }

    /**
     * вызов тестов после деплоя
     *
     * @return boolean
     */
    public function afterTests($isSilent = false, $isBoolen = true)
    {
        $directory = static::getConfig('afterTestsDirectory');
        $results = static::getInstance()->callbackFiles($directory);

        if (in_array('0', $results)) {
            if (!$isSilent) {
                throw new TestsFailed($results);
            }
            return false;
        }

        static::$_testsData = $results;
        return true;
    }

    /**
     * Запуск файлов из директории
     *
     * @param string $directory
     * @return array
     * @throws FileIsNotReadable
     * @throws \Deployment\Exception\LoggedException
     * @throws \Deployment\Exception\NotFoundFile
     */
    protected function callbackFiles(string $directory, $isSkipNotCallable = true)
    {
        $results = [];
        if ($directory) {
            Directory::setCurrentDirectory($directory);
            if ($files = Directory::files($directory, '/(:?.*)\.php$/')) {
                foreach ($files as $file) {
                    if (!is_readable($directory . DIRECTORY_SEPARATOR . $file)) {
                        throw new FileIsNotReadable($directory . DIRECTORY_SEPARATOR . $file);
                    }
                    if (is_file($directory . DIRECTORY_SEPARATOR . $file)) {
                        /**
                         * запуск файла теста
                         */
                        $callback = include($file);
                        if (is_callable($callback)) {
                            $result = $callback(static::getConfig());
                            $results[$file] = intval($result);
                        } elseif (!$isSkipNotCallable) {
                            throw new DataIsNotCallable($callback);
                        }
                    }
                }
            }
        }

        return $results;
    }

    /**
     * вызов тестов после деплоя
     *
     * @return boolean
     */
    public function beforeTests($isSilent = false, $isBoolen = true)
    {
        $directory = static::getConfig('beforeTestsDirectory');
        $results = static::callbackFiles($directory);

        /**
         * проверка на непройденные тесты
         */
        if (in_array('0', $results)) {
            if (!$isSilent) {
                throw new TestsFailed($results);
            }
            return $isBoolen ? false : $results;
        }

        static::$_testsData = $results;
        return $isBoolen ? true : $results;
    }

    /**
     * @return array
     */
    public function getTestsData()
    {
        return static::$_testsData ? static::$_testsData : [];
    }


    /**
     * установки развертывания проекта
     *
     * @return array
     */
    public function installProject()
    {
        Logger::Log(sprintf("Deploy::install(%s)", $this->procname));
    }
}