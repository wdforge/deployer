<?php

namespace Deployment\Deploy;

use \Deployment\Facade\Core\Api\Application;
use \Deployment\Facade\Core\Configuration;

/**
 * Class DeployController
 * @package Deployment\Core\Api
 */
class Controller extends Configuration
{
    /**
     * Деплой последней версии
     */
    public function upgrade()
    {
        Application::getTaskClient()->doBackground('dispatch', json_encode([
            'class' => Configuration::getConfig('deployClass'),
            'method' => 'upgrade'
        ]));
        if (Application::getTaskClient()->returnCode() != GEARMAN_SUCCESS) {
            return ['status' => false];
        }

        return ['status' => true];
    }

    /**
     * Настройки условий развертывания проекта
     */
    public function installDeploy()
    {
        Application::getTaskClient()->doBackground('dispatch', json_encode([
            'class' => Configuration::getConfig('deployClass'),
            'method' => 'installDeploy'
        ]));
        if (Application::getTaskClient()->returnCode() != GEARMAN_SUCCESS) {
            return ['status' => false];
        }

        return ['status' => true];
    }

    /**
     * Возврат проекта к предыдущей миграции БД
     */
    public function downgradeMigrations()
    {
        Application::getTaskClient()->doBackground('dispatch', json_encode([
            'class' => Configuration::getConfig('deployClass'),
            'method' => 'downgradeMigrations'
        ]));
        if (Application::getTaskClient()->returnCode() != GEARMAN_SUCCESS) {
            return ['status' => false];
        }

        return ['status' => true];
    }


    /**
     * Обновление миграций БД
     */
    public function upgradeMigrations()
    {
        Application::getTaskClient()->doBackground('dispatch', json_encode([
            'class' => Configuration::getConfig('deployClass'),
            'method' => 'upgradeMigrations'
        ]));
        if (Application::getTaskClient()->returnCode() != GEARMAN_SUCCESS) {
            return ['status' => false];
        }

        return ['status' => true];
    }


    /**
     * Откат данных БД на предыдущую версию
     */
    public function downgradeDump()
    {
        Application::getTaskClient()->doBackground('dispatch', json_encode([
            'class' => Configuration::getConfig('deployClass'),
            'method' => 'downgradeDump'
        ]));
        if (Application::getTaskClient()->returnCode() != GEARMAN_SUCCESS) {
            return ['status' => false];
        }

        return ['status' => true];
    }

    /**
     * Откат файлов на шаг назад
     */
    public function downgradeDeploy()
    {
        Application::getTaskClient()->doBackground('dispatch', json_encode([
            'class' => Configuration::getConfig('deployClass'),
            'method' => 'downgradeDeploy'
        ]));
        if (Application::getTaskClient()->returnCode() != GEARMAN_SUCCESS) {
            return ['status' => false];
        }

        return ['status' => true];
    }


    /**
     * Откат к предыдущей версии
     */
    public function downgrade()
    {
        Application::getTaskClient()->doBackground('dispatch', json_encode([
            'class' => Configuration::getConfig('deployClass'),
            'method' => 'downgrade'
        ]));

        if (Application::getTaskClient()->returnCode() != GEARMAN_SUCCESS) {
            return ['status' => false];
        }

        return ['status' => true];
    }

    /**
     * @future
     * ещё не работает((
     * откат к определенному состоянию
     */
    public function setgrade()
    {
        Application::getTaskClient()->doBackground('dispatch', json_encode([
            'class' => Configuration::getConfig('deployClass'),
            'method' => 'setgrade'
        ]));

        if (Application::getTaskClient()->returnCode() != GEARMAN_SUCCESS) {
            return ['status' => false];
        }

        return ['status' => true];
    }

    /**
     * Проверка последнего деплоя на корректность
     * Возможно *позднее запуск тестов
     *
     * @return bool
     */
    public function executeTests() {
        Application::getTaskClient()->doBackground('dispatch', json_encode([
            'class' => Configuration::getConfig('deployClass'),
            'method' => 'executeTests'
        ]));

        if (Application::getTaskClient()->returnCode() != GEARMAN_SUCCESS) {
            return ['status' => false];
        }

        return ['status' => true];
    }

}