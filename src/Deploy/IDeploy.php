<?php

namespace Deployment\Deploy;

/**
 * Interface IDeploy
 * @package Deployment\Core
 */
interface IDeploy {
    /**
     * обновление проекта до последней версии
     *
     * @return mixed
     */
    public function upgrade();

    /**
     * откат проекта на предыдущую версию
     *
     * @return mixed
     */
    public function downgrade();

    /**
     * переключение проекта на выбранную версию
     *
     * @return mixed
     */
    public function setgrade();

}