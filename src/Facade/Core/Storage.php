<?php

namespace Deployment\Facade\Core;

use Deployment\Exception\LoggedException;

/**
 * Class Storage
 * @package Deployment
 */
class Storage extends Configuration
{
    protected static $db = [];

    /**
     * Запись результатов в хранилище
     *
     * @param array $data
     * @return bool|int
     * @throws \Exception
     */
    public static function store($data = [])
    {
        $config = self::getConfig();
        if (isset($config['storageFile']) && isset($config['storageFile'])) {
            $dir = dirname($config['storageFile']);
            if (!is_dir($dir)) {
                if (!mkdir($dir, 0777, true)) {
                    throw new \Exception(sprintf("Не удаётся создать фаил хранилища: %s.", $dir));
                }
            }

            $dataSrc = "<?php\nreturn ";
            $dataSrc .= var_export($data, true).';';
            return file_put_contents($config['storageFile'], $dataSrc);
        }
        else {
            throw new LoggedException('Storage configuration is not settings');
        }

        return false;
    }

    /**
     * Загрузка данных из хранилища
     *
     * @return bool|mixed
     */
    public static function load()
    {
        $config = self::getConfig();
        if (isset($config['storageFile']) &&
            isset($config['storageFile']) &&
            is_readable($config['storageFile'])) {
            return include $config['storageFile'];
        }
        return false;
    }

    /**
     *
     */
    public static function autoload() {
        if(!static::$db) {
            static::$db = static::load();
        }
    }
    /**
     * Запись значения в хранилище
     *
     * @param string $key
     * @param mixed $value
     * @return bool|void
     */
    public static function setSection(string $key, $value)
    {
        static::autoload();

        static::$db[$key] = $value;
        return static::store(static::$db);
    }

    /**
     * Запись значений в хранилище
     *
     * @param string $key
     * @param $value
     * @return bool|void
     */
    public static function pushSection(string $key, $value)
    {
        static::autoload();

        if (!isset(static::$db[$key]) || !is_array(static::$db[$key])) {
            static::$db[$key] = [];
        }

        static::$db[$key][] = $value;

        return static::store(static::$db);
    }

    /**
     * Получение значения по ключу
     *
     * @param string $key
     * @return mixed|null
     */
    public static function getSection(string $key) {
        if(static::hasSection($key)) {
            return static::$db[$key];
        }
        return null;
    }

    /**
     * Проверка на наличие секции
     *
     * @param string $key
     * @return bool
     */
    public static function hasSection(string $key) {

        static::autoload();

        return isset(static::$db[$key]);
    }

    /**
     * Получение значения по секции и идентификатору
     *
     * @param string $sectionKey
     * @param $recordId
     * @return null
     */
    public static function getById(string $sectionKey, $recordId) {
        if($section  = static::getSection($sectionKey)) {
            return isset($section[$recordId]) ? $section[$recordId]: null;
        }
        return null;
    }

    /**
     * Установка значения в записи
     *
     * @param string $sectionKey
     * @param $recordId
     * @param null $value
     */
    public static function setById(string $sectionKey, $recordId, $value = null) {
        static::$db[$sectionKey][$recordId] = $value;
        return static::store(static::$db);
    }

}