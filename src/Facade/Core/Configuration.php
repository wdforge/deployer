<?php

namespace Deployment\Facade\Core;

use Deployment\Exception\NotFoundFile;
use Deployment\NullObject;

/**
 * Class Configuration
 * @package Deployment\Core
 */
class Configuration
{
    protected static $_config = [];
    protected static $_filePath = __DIR__ . "/../config.php";


    /**
     * Получение отдельной секции или списка всех параметров
     *
     * @param string $section
     * @return array|mixed|null
     * @throws NotFoundFile
     */
    public static function getConfig($section = "")
    {
        if (empty(static::$_config)) {
            static::loadConfig(static::$_filePath);
        }
        if (empty($section)) {
            return static::$_config;
        }

        return isset(static::$_config[$section]) ? static::$_config[$section] : null;
    }

    /**
     * Загрузка нового конфиг-массива
     *
     * @param string $filePath
     * @return array
     */
    public static function loadConfig($filePath)
    {
        if (is_null(static::$_config)) {
            static::$_config = [];
        }

        if (file_exists($filePath) && is_readable($filePath)) {
            $new_config = include_once($filePath);
            if (is_array($new_config)) {
                static::$_config = static::mergeArray(static::$_config, $new_config);
            }
        } else {
            throw new NotFoundFile($filePath);
        }

        return static::getConfig();
    }

    /**
     * Метод слияния массивов
     *
     * @param $Arr1
     * @param $Arr2
     * @return array
     */
    protected static function mergeArray(array $Arr1, array $Arr2)
    {
        $MergeArrays = function ($Arr1, $Arr2) use (&$MergeArrays) {
            foreach ($Arr2 as $key => $Value) {
                if (array_key_exists($key, $Arr1) && is_array($Value))
                    $Arr1[$key] = $MergeArrays($Arr1[$key], $Arr2[$key]);
                else if (is_int($key))
                    $Arr1[] = $Value;
                else
                    $Arr1[$key] = $Value;

            }
            return $Arr1;
        };

        return $MergeArrays($Arr1, $Arr2);
    }

    /**
     * Cлияние конфига со стартовыми параметрами скрипта
     *
     * @param $argv
     * @return array
     */
    protected function mergeCommands($argv)
    {
        array_shift($argv);
        $out = array();
        foreach ($argv as $arg) {
            if (substr($arg, 0, 2) == '--') {
                $eqPos = strpos($arg, '=');
                if ($eqPos === false) {
                    $key = substr($arg, 2);
                    $out[$key] = isset($out[$key]) ? $out[$key] : true;
                } else {
                    $key = substr($arg, 2, $eqPos - 2);
                    $out[$key] = substr($arg, $eqPos + 1);
                }
            } else if (substr($arg, 0, 1) == '-') {
                if (substr($arg, 2, 1) == '=') {
                    $key = substr($arg, 1, 1);
                    $out[$key] = substr($arg, 3);
                } else {
                    $chars = str_split(substr($arg, 1));
                    foreach ($chars as $char) {
                        $key = $char;
                        $out[$key] = isset($out[$key]) ? $out[$key] : true;
                    }
                }
            } else {
                $out[] = $arg;
            }
        }

        static::mergeArray(static::$_config, $out);
        return $out;
    }

    /**
     * Проксирование доступа через свойства к параметрам конфига
     *
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return isset(static::$_config[$name]) ? static::$_config[$name] : null;
    }

    /**
     * Проксирование выполнения несуществующих методов
     *
     * @param $name
     * @param $arguments
     * @return NullObject
     */
    public function __call($name, $arguments)
    {
        return NullObject::create($name);
    }
}