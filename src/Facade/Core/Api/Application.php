<?php

namespace Deployment\Facade\Core\Api;

use Deployment\Facade\Core\Configuration;
use Deployment\ErrorHandler;
use Deployment\Exception\LoggedException;
use Deployment\Traits\Events;

/**
 * Class Application
 * @package Deployment\Core\Api
 */
class Application extends Configuration
{
    use Events;

    protected static $_errorManager;

    /**
     * @var GearmanClient;
     */
    protected static $_client;

    /**
     *
     */
    public static function init()
    {
        try {
            if (static::getConfig('isHookError')) {
                static::$_errorManager = new ErrorHandler();
                static::$_errorManager->init();
            }

            /**
             * инициализация GearmanClient
             */
            if (!class_exists('GearmanClient')) {
                throw new LoggedException(sprintf("Gearman is not supported"));
            }

            $gearmanPort = static::getConfig('gearmanPort');
            $gearmanServer = static::getConfig('gearmanServer');

            if (!empty($gearmanPort) && !empty($gearmanServer)) {
                static::$_client = new \GearmanClient();
                static::$_client->addServer($gearmanServer, $gearmanPort);
            } else {
                throw new LoggedException('Uncomplete Gearman settings');
            }

            Router::init();
            $result = Router::route();
        } catch (\Exception $e) {
            throw new $e;
        }

        echo json_encode($result);
        exit;
    }

    /**
     * @return GearmanClient
     */
    public static function getTaskClient()
    {
        return static::$_client;
    }

}