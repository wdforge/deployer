<?php

namespace Deployment\Facade\Core;

use Deployment\Exception\FileIsNotWriteable;

/**
 * Class Logger
 * @package Deployment\Core
 */
class Logger extends Configuration
{
    /**
     * Запись строки в лог или вывод
     *
     * @param $log
     * @throws FileIsNotWriteable
     * @throws \Deployment\Exception\NotFoundFile
     */
    public static function Log($log)
    {
        if (!static::getConfig('isLogging')) {
            echo "\n" . $log;
            return;
        }

        if (static::getConfig('logfile')) {
            $dir = dirname(static::getConfig('logfile'));

            if (!is_dir($dir)) {
                if (!mkdir($dir, 0777, true)) {
                    throw new FileIsNotWriteable($dir);
                }
            }

            if (!empty($log) && is_string($log) && !empty(static::getConfig('logfile')) && $file = fopen(static::getConfig('logfile'), "a")) {
                $date_str = date('Y-m-d H:i:s');
                fwrite($file, "\n" . $date_str . ":  " . $log . "\n");
                fclose($file);
            }
        } else {
            echo "\n" . $log;
        }
    }
}