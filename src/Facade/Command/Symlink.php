<?php

namespace Deployment\Facade\Command;

use Deployment\Exception\LoggedException;

/**
 * Class Symlink
 * @package Deployment\Command
 */
class Symlink
{
    /**
     * логирование добавленных симлинков
     *
     * @var array
     */
    protected static $links = [];

    /**
     * добавление символической ссылки
     *
     * @param string $filePathFrom
     * @param string $filePathTo
     * @param bool $isHard
     */
    public static function addSymlink(string $filePathFrom, string $filePathTo)
    {
        if (file_exists($filePathFrom)) {
            $result = Shell::command(sprintf("ln -sf %s %s", $filePathFrom, $filePathTo));
            if (!$result[1]) {
                static::$links[$filePathTo] = $filePathFrom;
            }
            return !$result[1];
        }

        throw new LoggedException(sprintf("File %s is not exists.", $filePathFrom));
    }

    /**
     * удаление символической ссылки
     *
     * @param string $filePath
     */
    public static function removeSymlink(string $filePath)
    {
        $result = null;
        try {
            if (file_exists($filePath)) {
                if (is_link($filePath)) {
                    $result = Shell::command(sprintf("rm %s -rf -R", $filePath));
                    return !$result[1];
                }
                throw new LoggedException(sprintf("File %s is not symlink.\nConsole: %s\n", $filePath, $result[0]));
            }
            throw new LoggedException(sprintf("File %s is not exists.\nConsole: %s\n", $filePath, $result[0]));
        }
        catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * если символическая ссылка
     *
     * @param string $filePath
     */
    public static function isSymlink(string $filePath)
    {
        if (file_exists($filePath)) {
            if (is_link($filePath)) {
                return true;
            }
        }

        return false;
    }

}