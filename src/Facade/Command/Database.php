<?php

namespace Deployment\Facade\Command;

use Deployment\Facade\Core\Configuration;

class Database extends Configuration
{
    /**
     * @var Database
     */
    protected static $_instance;

    /**
     * @var resource $_resource
     */
    protected static $_resource;

    /**
     * Подключение к бд
     *
     * @param string $host
     * @param string $user
     * @param string $password
     * @param string $database
     */
    public static function connect(string $host, string $user, string $password, string $database)
    {
        if (!static::$_resource) {
            try {
                static::$_resource = new \PDO(sprintf('mysql:host=%s;dbname=%s', $host, $database), $user, $password);
            } catch (\PDOException $e) {
                throw $e;
            }

            register_shutdown_function(function () {
                if (static::$_resource) {
                    static::$_resource = null;
                }
            });

        }
    }

    /**
     * выполнение запроса на select
     *
     * @param string|null $sqlMask
     * @param array $values
     * @return mixed
     */
    public static function select(string $sqlMask = null, array $values = [])
    {
        try
        {
            $stmt = static::$_resource->prepare($sqlMask, $values);
            $stmt->execute($values);
            return $stmt->fetchAll();
        }
        catch (\PDOException $e)
        {
            throw $e;
        }
    }


    /**
     * выполнение запроса на update
     *
     * @param string|null $sqlMask
     * @param array $values
     * @return mixed
     */
    public static function update(string $sqlMask = null, array $values = [])
    {
        try
        {
            $stmt = static::$_resource->prepare($sqlMask, $values);
            return $stmt->execute($values);
        }
        catch (\PDOException $e)
        {
            throw $e;
        }
    }

    /**
     * выполнение запроса на delete
     *
     * @param string|null $sqlMask
     * @param array $values
     * @return mixed
     */
    public static function delete(string $sqlMask = null, array $values = []) {
        return static::update($sqlMask, $values);
    }

    /**
     * выполнение запроса на insert
     *
     * @param string|null $sqlMask
     * @param array $values
     * @return mixed
     */
    public static function insert(string $sqlMask = null, array $values = []) {
        /**
         * если переданные параметры многомерный массив
         */
        $isMultirows = count($values) && (count($values) === count($values, COUNT_RECURSIVE)) ? false: boolval(count($values));

        try
        {
            if($isMultirows) {
                $stmt = static::$_resource->prepare($sqlMask);
                foreach($values as $record) {
                    $stmt->execute($record);
                }
            }
            else {
                return static::$_resource->prepare($sqlMask)->execute($values);
            }
        }
        catch (\PDOException $e)
        {
            throw $e;
        }

        return true;
    }

    /**
     *
     * @return static
     */
    public static function getInstance()
    {
        if (!static::$_instance) {
            static::$_instance = new static;
        }
        return static::$_instance;
    }


}