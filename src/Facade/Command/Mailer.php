<?php

namespace Deployment\Facade\Command;

/**
 * Class Mailer
 * @package Deployment\Command
 */
class Mailer extends Configuration
{
    /**
     * @param $title
     * @param $body
     */
    public static function mail($title, $body)
    {
        //... тут будет отправка почты
    }
}