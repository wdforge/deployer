<?php
namespace Deployment\Facade\Command;
use Deployment\Facade\Core\Configuration;

/**
 * Class MySQLDump
 */
class MySQLDump extends Configuration
{

    /**
     * создание дампа и архивация
     *
     * @param $archiveFile
     * @param $user
     * @param $password
     * @param $host
     * @param $database
     * @param int $type
     * @param array $excludeTables
     */
    public static function create($archiveFile, $user, $password, $host, $database, $options = ['--single-transaction', '--quick'])
    {
        $output = Shell::command(sprintf('mysqldump -u %s -p%s -h%s %s %s| gzip > %s',
            $user,
            $password,
            $host,
            $database,
            implode(" ", $options),
            $archiveFile
        ));

        if ($output[1] && !static::getConfig('isSilentDumpFail')) {
            throw new LoggedException(sprintf('Mysql dump compress failed: %s', $output[0]));
        }

        return $output[1];
    }

    /**
     * загрузка дампа из архива
     *
     * @param $archiveFile
     * @param $user
     * @param $password
     * @param $host
     * @param $database
     * @return mixed
     * @throws \Deployment\Exception\BadParameters
     * @throws \Deployment\Exception\NotFoundFile
     */
    public static function import(string $archiveFile, string $user, string $password, string $host, string $database = null)
    {
        if($database) {
            $output = Shell::command(sprintf('gunzip < %s | mysql -u %s -p%s -h%s %s',
                $archiveFile,
                $user,
                $password,
                $host,
                $database
            ));
        }
        else {
            $output = Shell::command(sprintf('gunzip < %s | mysql -u %s -p%s -h%s',
                $archiveFile,
                $user,
                $password,
                $host
            ));
        }

        if ($output[1] && !static::getConfig('isSilentDumpFail')) {
            throw new LoggedException(sprintf('Mysql dump import failed: %s', $output[0]));
        }

        return $output[1];
    }

    /**
     *
     */
    public static function command(string $sqlQuery, string $user, string $password, string $host, string $database = null)
    {
        if($database) {
            $output = Shell::command(sprintf('mysql -u %s -p%s -h%s %s -e\'%s\'',
                $user,
                $password,
                $host,
                $database,
                $sqlQuery
            ));
        }
        else {
            $output = Shell::command(sprintf('mysql -u %s -p%s -h%s -e\'%s\'',
                $user,
                $password,
                $host,
                $sqlQuery
            ));
        }

        if ($output[1] && static::getConfig('isInterruptSqlFail')) {
            throw new LoggedException(sprintf('Mysql SQL error: %s', $output[0]));
        }

        return $output[1];

    }
}