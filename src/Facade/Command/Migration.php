<?php

namespace Deployment\Facade\Command;

use Deployment\Deploy\IDeploy;
use Deployment\Exception\FileIsNotReadable;
use Deployment\Exception\FileIsNotWriteable;
use Deployment\Exception\LoggedException;
use Deployment\Exception\NotFoundFile;
use Deployment\Facade\Core\Configuration;
use Deployment\Facade\Core\Logger;
use Deployment\Facade\Core\Storage;

/**
 * Class Migration
 *
 * @package Deployment\Command
 * @future Класс для управления миграциями и автоматическим расчетом шагов для отката
 * @status in the design state
 *
 */
class Migration extends Configuration implements IDeploy
{

    protected static $_migrator = '/vendor/robmorgan/phinx/bin/phinx';
    protected static $_migrationPath = '/database';
    protected static $_instance;

    /**
     * обновление миграции проекта
     */
    public function upgrade()
    {
        $result = null;

        $gitProjectPath = $this->deployGitDirectory . DIRECTORY_SEPARATOR . Git::getProjectName($this->gitRepository).
            DIRECTORY_SEPARATOR.static::$_migrationPath;

        Directory::setCurrentDirectory($gitProjectPath);

        if ($fexists = file_exists($this->deployDirectory . static::$_migrator)) {
            $result = Shell::command('/usr/bin/php ' . $this->deployDirectory . '/vendor/robmorgan/phinx/bin/phinx migrate');
        }
        if ((isset($result) && !empty($result[1])) || !$result) {
            throw new LoggedException(sprintf('Migration failed: %s', $result[0]));
        }
        return $result;
    }

    /**
     * откат миграции проекта
     */
    public function downgrade()
    {
        $result = null;

        $gitProjectPath = $this->deployGitDirectory . DIRECTORY_SEPARATOR . Git::getProjectName($this->gitRepository).
            DIRECTORY_SEPARATOR.static::$_migrationPath;

        Directory::setCurrentDirectory($gitProjectPath);

        if ($fexists = file_exists($this->deployDirectory . '/vendor/robmorgan/phinx/bin/phinx')) {
            $result = Shell::command('/usr/bin/php ' . $this->deployDirectory . '/vendor/robmorgan/phinx/bin/phinx rollback');
        }

        if ((isset($result) && !empty($result[1])) || !$result) {
            throw new LoggedException(sprintf('Migration downgrade failed: %s', $result[0]));
        }

        return $result;
    }

    /**
     *
     * @return \Deployment\Deploy
     */
    public static function getInstance()
    {
        if (!static::$_instance) {
            static::$_instance = new static;
        }

        return static::$_instance;
    }

    /**
     * @return mixed|void
     */
    public function setgrade()
    {

    }

    /**
     * Получение id миграции по хэшу комита
     *
     * @param string|null $commitHash
     */
    public static function getVersion(string $commitHash = null)
    {
        $migrations = Storage::getSection('migration');

        if($migrations && array_key_exists($commitHash, $migrations)) {
            return $migrations[$commitHash];
        }

        return false;
    }

    /**
     * Добавление значения о поd
     *
     * @param string $commitHash
     * @param int $id
     */
    public static function setLastVersion(string $commitHash, string $id = null)
    {
        if(!$migrations = Storage::getSection('migrations')) {
            $migrations = [];
        }

        $migrations = array_merge($migrations, [$commitHash => $id]);
        Storage::setSection('migrations', $migrations);
    }

    /**
     * Проверка наличия миграции в списке
     *
     * @param string $id
     * @return bool
     */
    public static function has(string $id)
    {
        $migrations = [];

        if($migrations = Storage::getSection('migrations') && in_array($id, $migrations)) {
            return true;
        }

        return false;
    }

    /**
     * Получение идентификатора из таблицы последней миграции
     */
    public static function getLastId()
    {
        $result = Database::select('select max(version) from migrations');

        if($rowset = reset($result)) {
            return end($rowset);
        }

        return false;
    }

    /**
     * Получение количества миграций проекта
     *
     * @return bool|integer
     */
    public static function getCountRecords()
    {
        $result = Database::select('select count(version) from migrations');

        if($rowset = reset($result)) {
            return end($rowset);
        }

        return false;
    }

    /**
     * Получение количества шагов для отката к определенному комиту
     *
     * @param string|null $commitHash
     */
    public static function getStepCount(string $commitHash = null)
    {
        $step = 0;

        if($migrations = Storage::getSection('migrations')) {

            /**
             * Развернули список миграций и смотрим с конца
             */
            foreach (array_reverse($migrations) as $commitHashStore => $migrationId) {
                if ($commitHash === $commitHashStore) {
                    break;
                }
            }
        }

        return $step;
    }

    /**
     * Предыдущая миграция
     * hash=>id
     *
     * @param string $commitHash
     */
    public static function getPrevious(string $commitHash)
    {
        /**
         * @var $migrations array
         */
        $migrations = Storage::getSection('migrations');

        if($migrations && count($migrations) > 1) {

            $hashs = array_keys($migrations);
            $hash = array_shift($hashs);
            $migrationId = array_shift($migrations);

            return [
                $hash => $migrationId
            ];
        }

        return false;
    }

}