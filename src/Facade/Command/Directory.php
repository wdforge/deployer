<?php

namespace Deployment\Facade\Command;

use Deployment\Exception\FileIsNotReadable;
use Deployment\Exception\FileIsNotWriteable;
use Deployment\Exception\LoggedException;
use Deployment\Exception\NotFoundFile;
use Deployment\Facade\Core\Logger;

/**
 * Class Directory
 * @package Deployment\Command
 */
class Directory
{
    /**
     * Создание директории
     *
     * @param string $path
     * @param int $mode
     * @return bool
     * @throws FileIsNotWriteable
     * @throws NotFoundFile
     */
    public static function create(string $path, $mode = 0736)
    {
        if(!file_exists($path)) {
            Logger::Log(sprintf("Create directory: %s", $path));
            return mkdir($path, $mode, true);
        }
        return false;
    }

    /**
     * Получение более верхнего уровня пути
     *
     * @param string $path
     * @return string
     */
    public static function getParentPath(string $path)
    {
        $dirLst = explode(DIRECTORY_SEPARATOR, $path);
        unset($dirLst[count($dirLst)-1]);
        $dirLst = array_merge($dirLst, []);
        $prevDir = implode('/', $dirLst);
        return $prevDir;
    }

    /**
     * Удаление файла/директории
     *
     * @param string $path
     */
    public static function delete(string $path)
    {
        Logger::Log(sprintf("Delete files: %s", $path));
        Shell::exec(sprintf('chmod 0777 %s -R', $path));
        $result = Shell::exec(sprintf('rm -rfd %s', $path));
        return !$result[1];
    }

    /**
     * Копирование файла/директории
     *
     * @param $pathFrom
     * @param $pathTo
     * @return bool
     */
    public static function copy($pathFrom, $pathTo) {

        Logger::Log(sprintf("Copy path From: %s, To: %s", $pathFrom, $pathTo));

        if (file_exists($pathTo)) {
            static::delete($pathTo);
        }
        if (is_dir($pathFrom)) {
            if(mkdir($pathTo)) {
                $files = scandir($pathFrom);
                foreach ($files as $file) {
                    if ($file != "." && $file != "..") {
                        static::copy("$pathFrom/$file", "$pathTo/$file");
                    }
                }
            }
            else {
                throw new FileIsNotWriteable($pathTo);
            }
        }
        else if (file_exists($pathFrom)) {
            if(!copy($pathFrom, $pathTo)) {
                throw new FileIsNotWriteable($pathTo);
            }
        }
        else {
            throw new NotFoundFile($pathFrom);
        }
        return true;
    }

    /**
     * Перемещение файла/директории
     *
     * @param string $pathFrom
     * @param string $pathTo
     */
    public static function move(string $pathFrom, string $pathTo)
    {
        Logger::Log(sprintf("Move files: From %s, To: %s", $pathFrom, $pathTo));

        if(static::copy($pathFrom, $pathTo)) {
            return static::delete($pathFrom);
        }
        return false;
    }

    /**
     * Получение списка файлов в директории
     *
     * @param string $path
     * @param string|null $pattern
     * @return array
     */
    public static function files(string $path, string $pattern = null)
    {
        $result = [];
        if (is_dir($path)) {
            if ($handle = opendir($path)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    $result[] = $file;
                }
                closedir($handle);
                if($pattern) {
                    $result = preg_grep($pattern, $result);
                }
            }
        }
        return $result;
    }

    /**
     * Установка текущей директории
     *
     * @param string $path
     * @return bool
     * @throws LoggedException
     */
    public static function setCurrentDirectory(string $path)
    {
        if (file_exists($path)) {
            if (is_readable($path)) {
                return chdir($path);
            } else {
                throw new FileIsNotReadable($path);
            }
        }

        throw new NotFoundFile($path);
    }

    /**
     * Получение текущего каталога
     *
     * @return string
     */
    public static function getCurrentDirectory()
    {
        return getcwd();
    }

    /**
     * Установка прав для каталога и файлов внтури
     *
     * @param string $path
     * @param integer $rules
     */
    public static function setChmod(string $path, $rules = 0775)
    {
        $result = chmod($path, $rules);
        Shell::exec(sprintf("find %s -type d -exec chmod %o {} +", $path, $rules));
        Shell::exec(sprintf("find %s -type f -exec chmod %o {} +", $path, $rules));
        return $result;
    }

}