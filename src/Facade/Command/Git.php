<?php

namespace Deployment\Facade\Command;

use Deployment\Exception\LoggedException;
use Deployment\Facade\Core\Configuration;
use Deployment\Facade\Core\Logger;

/**
 * Класс оболочка для управления командами гита
 *
 * Class Git
 * @package Deployment
 */
class Git extends Configuration
{
    /**
     * Клонирование проекта перед развертыванием
     *
     * @param string|null $url
     * @param string|null $pathTo
     * @return string
     * @throws LoggedException
     */
    public static function clone(string $url = null, string $pathTo = null)
    {
        $config = static::getConfig();

        $pathTo = $pathTo ? $pathTo : (!empty($config['deployGitDirectory']) ? $config['deployGitDirectory'] : null);
        $url = $url ? $url : (isset($config['gitRepository']) ? $config['gitRepository'] : null);

        if (!$url) {
            throw new LoggedException(sprintf('Repository is not set'));
        }

        if (!$pathTo) {
            throw new LoggedException(sprintf('Repository path is not set'));
        }

        /**
         * переход в директории где будет хранится склонированный проект
         */
        Directory::setCurrentDirectory($pathTo);

        /**
         * новый путь проекта после будущего клонирования
         */
        $newDirectory = Directory::getCurrentDirectory() . '/' . static::getProjectName($url);

        /**
         * удаление существующего пути, иначе клонирование не пройдет
         */
        if (file_exists($newDirectory)) {
            Logger::Log(sprintf('Deleting exists files in: %s', $newDirectory));
            Directory::delete($newDirectory);
        }

        $gitClone = function () use ($url, $config, $newDirectory) {
            Logger::Log(sprintf("Cloning project: %s", $url));
            Shell::command(sprintf('git clone %s', $url));
            return $newDirectory;
        };

        return $gitClone();
    }

    /**
     * Клонирование и переключение репозитория на определенный комит
     *
     * @param string $pathTo
     * @param string $commitHash
     */
    public static function setCurrentCommit($pathTo = null, string $commitHash, bool $isClonning = true, string $url = "")
    {
        $config = static::getConfig();

        $pathTo = $pathTo ? $pathTo : (!empty($config['deployGitDirectory']) ? $config['deployGitDirectory'] : null);
        $url = $url ? $url : (isset($config['gitRepository']) ? $config['gitRepository'] : null);

        if (!$commitHash) {
            throw new LoggedException(sprintf('Commit hash is not set'));
        }

        if (!$url && $isClonning) {
            throw new LoggedException(sprintf('Repository is not set'));
        }

        if (!$pathTo) {
            throw new LoggedException(sprintf('Repository path is not set'));
        }

        if ($isClonning) {
            $projectDirectory = static::clone($url, $pathTo);
            Directory::setCurrentDirectory($projectDirectory);
        } else {
            Directory::setCurrentDirectory($pathTo);
        }

        $result = Shell::exec(sprintf('git checkout %s', $commitHash));

        return !$result[1];
    }

    /**
     * возвращает хэш текущего комита
     *
     * @param $path
     * @return bool|string
     * @throws LoggedException
     */
    public static function getCurrentCommit($path)
    {
        Directory::setCurrentDirectory($path);
        $result = Shell::command('git show -s --format=%H');
        if (!$result[1]) {
            return trim(implode("", $result[0]));
        }

        return false;
    }

    /**
     * Получение "хвоста" ссылки, при клонировании это имя директории проекта
     *
     * @param $url
     * @return mixed
     */
    public static function getProjectName($url)
    {
        $path = explode('/', $url);
        return str_replace('.git', '', end($path));
    }
}