<?php

namespace Deployment\Facade\Command;

/**
 * Class Composer
 * @package Deployment\Command
 */
class Composer
{
    /**
     * @return bool
     * @throws \Deployment\Exception\BadParameters
     */
    public static function update()
    {
        $result = Shell::command('composer update');
        return !$result[1];
    }

    /**
     * @return bool
     * @throws \Deployment\Exception\BadParameters
     */
    public static function install()
    {
        $result = Shell::command('composer install');
        return !$result[1];
    }

}