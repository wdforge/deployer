<?php

namespace Deployment\Facade\Command;

use Deployment\Exception\BadParameters;

/**
 * Class Shell
 * @package Deployment\Command
 */
class Shell
{
    /**
     * запуск приложения
     *
     * @param string $pathParams
     * @return bool
     */
    public static function exec(string $cmd)
    {
        $pid = pcntl_fork();
        if ($pid < 0) {
            return -1;
        }
        if ($pid == 0) {
            pcntl_exec('/bin/sh', array('-c', $cmd), $_ENV);
            exit(127);
        }

        pcntl_waitpid($pid, $status);
        return pcntl_wexitstatus($status);
    }

    /**
     * запуск bash-string
     *
     * @param string $commandString
     */
    public static function command(string $cmd)
    {
        $output = "";
        $result = 0;

        exec($cmd, $output, $result);

        if ($result) {
            throw new BadParameters($cmd);
        }

        return [
            $output,
            $result
        ];

    }

}