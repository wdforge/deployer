Команды управления развёртыванием проекта


Сайт проекта:
http://vitrina.wdf.su

Алгоритм обновления:

1 - обновление файлов в проекте
2 - создание архива с дампом бд
3 - выполнение миграции базы

Ссылка обновления:
http://vitrina-deploy.wdf.su/vitrina/upgrade


Алгоритм отката на предыдущую версию:

1 - возврат к предыдущей файловой структуре
2 - распаковка и загрузка дампа базы данных (возврат старых данных)

Ссылка отката:
http://vitrina-deploy.wdf.su/vitrina/downgrade


Дополнительные команды
----------------------------------------------------------------------------------

Откат базы данных до предыдущего состояния (выполняется вручную):
http://vitrina-deploy.wdf.su/vitrina/downgradeDump

Выполнение миграции (выполняется автоматически при Upgrade):
http://vitrina-deploy.wdf.su/vitrina/upgradeMigrations

Откат миграции (выполняется автоматически при Downgrade):
http://vitrina-deploy.wdf.su/vitrina/downgradeMigrations

Запуск выполнения тестов проекта (выполняется автоматически при Upgrade):
http://vitrina-deploy.wdf.su/vitrina/executeTests

Воврат файлов к предыдущей версии деплоя  (выполняется автоматически при Downgrade):
http://vitrina-deploy.wdf.su/vitrina/downgradeDeploy
