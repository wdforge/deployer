<?php
/**
 * Тестовый тест на проверку успешности проведения деплоя
 */
return function(array $config) {
    echo sprintf("\nExecute test: %s for %s\n", __FILE__, isset($config['procname']) ? $config['procname']: 'unknown');
    return true;
};
