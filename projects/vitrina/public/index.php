<?php
/**
 * Код для вызова API проекта (вызов задач через Gearman)
 */
include __DIR__."/../../../vendor/autoload.php";

/**
 * загрузка конфига
 */
\Deployment\Facade\Core\Configuration::loadConfig(__DIR__.'/../config.php');

/**
 * вызов обработки приложения
 */
\Deployment\Facade\Core\Api\Application::init();