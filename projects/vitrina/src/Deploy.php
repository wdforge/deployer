<?php

namespace Vitrina;

use Deployment\Exception\FileIsNotWriteable;
use Deployment\Exception\NotFoundFile;
use Deployment\Facade\Command\Composer;
use Deployment\Facade\Command\Database;
use Deployment\Facade\Command\Directory;
use Deployment\Facade\Command\Migration;
use Deployment\Facade\Command\Shell;
use Deployment\Facade\Command\MySQLDump;
use Deployment\Exception\BadParameters;
use Deployment\Exception\LoggedException;
use Deployment\Facade\Command\Git;
use Deployment\Facade\Core\Storage;
use Deployment\Facade\Command\Symlink;

/**
 * Class Deploy
 * класс для управлением развертыванием проекта Витрина
 *
 * @package Deployment
 *
 * @property $deployGitDirectory string
 * @property $deployDirectory string
 * @property $deploySourceDirectory string
 * @property $deployStaticDirectory string
 * @property $deployArchiveDirectory string
 * @property $gitRepository string
 * @property $gitCommit string
 * @property $gitBranch string
 * @property $procname
 * @property $dumpDirectory
 */
class Deploy extends \Deployment\Deploy\Deploy
{
    /**
     * Тут задается специфика развертывания проекта
     *
     * @throws LoggedException
     */
    public function upgrade()
    {
        /**
         * Имя директории где будет хранится Исходный код проекта
         */
        $deployName = $this->procname . '_' . date('Ymd_His');
        $timeAdd = date('Y-m-d H:i:s');

        /**
         * Клонирование с заменой Исходного кода из GIT репозитория
         */
        Directory::setCurrentDirectory($this->deployGitDirectory);
        Git::setCurrentCommit(null, $this->gitCommit, true, $this->gitRepository);


        $gitProjectPath = $this->deployGitDirectory . DIRECTORY_SEPARATOR . Git::getProjectName($this->gitRepository);
        Directory::setCurrentDirectory($gitProjectPath);
        /**
         * Настройки базы данных
         */
        $dbSettingsFile = $gitProjectPath . "/config/connect_db.php";
        $dbSettings = file_exists($dbSettingsFile) ? include $dbSettingsFile : [];

        if (empty($dbSettings)) {
            throw new BadParameters('Database settings failed');
        }

        /**
         * Подключение к БД для миграции
         */
        Database::connect($dbSettings['host'], $dbSettings['user'], $dbSettings['pas'], $dbSettings['db_name']);

        /**
         * Обновление композера в проекте
         */
        Composer::update();

        /**
         * Получение Хэша последнего комита
         */
        $hashCommit = Git::getCurrentCommit($this->deployGitDirectory . DIRECTORY_SEPARATOR . Git::getProjectName($this->gitRepository));

        /**
         * Копирование Исходного кода из GIT-репозитория в директорию хранилище-деплоев
         */
        $sourcePath = $this->deploySourceDirectory . DIRECTORY_SEPARATOR . $deployName;
        Directory::copy(Directory::getCurrentDirectory(), $sourcePath);

        /**
         * Удаление не нужных файлов
         */
        Directory::delete($sourcePath . '/.git');
        Directory::delete($sourcePath . '/.gitignore');

        /**
         * Пересоздание символической ссылки на Исходный код проекта
         */
        if (Symlink::isSymlink($this->deployDirectory)) {
            Symlink::removeSymlink($this->deployDirectory);
        }
        Symlink::addSymlink($sourcePath, $this->deployDirectory);

        /**
         * Пересоздание симлинков внутри проекта
         * @future:
         *
         * 1) Планируется расширить фасад Symlink для работы со списками
         * 2) Выработать событийную схему выполнения деплоя, после чего вынести все последовательности операций в конфиг в привязке к событиям.
         */
        if (Symlink::isSymlink($this->deployDirectory . '/kcfinder-3.12')) {
            Symlink::removeSymlink($this->deployDirectory . '/kcfinder-3.12');
        }
        Symlink::addSymlink($this->deployStaticDirectory . '/kcfinder-3.12', $this->deployDirectory . '/kcfinder-3.12');

        if (Symlink::isSymlink($this->deployDirectory . '/services')) {
            Symlink::removeSymlink($this->deployDirectory . '/services');
        }
        Symlink::addSymlink($this->deployStaticDirectory . '/services', $this->deployDirectory . '/services');

        if (Symlink::isSymlink($this->deployDirectory . '/static')) {
            Symlink::removeSymlink($this->deployDirectory . '/static');
        }
        Symlink::addSymlink($this->deployStaticDirectory . '/static', $this->deployDirectory . '/static');

        if (Symlink::isSymlink($this->deployDirectory . '/baseHelpers/SxGeoCity.dat')) {
            Symlink::removeSymlink($this->deployDirectory . '/baseHelpers/SxGeoCity.dat');
        }
        Symlink::addSymlink($this->deployStaticDirectory . '/SxGeoCity.dat', $this->deployDirectory . '/baseHelpers/SxGeoCity.dat');

        if (Symlink::isSymlink($this->deployDirectory . '/media')) {
            Symlink::removeSymlink($this->deployDirectory . '/media');
        }
        Symlink::addSymlink($this->deployStaticDirectory . '/media', $this->deployDirectory . '/media');

        /**
         * создание архива с бампом бд
         */
        $archiveFile = sprintf('%s/%s_%s.sql.gz', static::getConfig('dumpDirectory'), $dbSettings['db_name'], date('Ymd_His'));
        MysqlDump::create($archiveFile, $dbSettings['user'], $dbSettings['pas'], $dbSettings['host'], $dbSettings['db_name']);


        /**
         * добавление в хранилище -> список деплоев
         */
        if ($deploys = Storage::getSection('deploys')) {

            /**
             * Удаление старых версий деплоя
             */
            if (count($deploys) >= static::getConfig('maxDeployCount')) {

                $deploy = array_shift($deploys);
                $deployName = end($deploy);

                $deployPath = $this->deploySourceDirectory . DIRECTORY_SEPARATOR . $deployName;
                Directory::delete($deployPath);

                Storage::setSection('deploys', $deploys);
            }
        }

        /**
         * запись нового значения
         */
        Storage::setById("deploys", $timeAdd, [
            $hashCommit => $deployName,
        ]);

        /**
         * выполнение миграции базы
         */
        $this->upgradeMigrations();

        /**
         * Добавление в хранилище -> список дампов
         */
        if ($dumps = Storage::getSection('dumps')) {

            /**
             * Удаление старых версий дампа
             */
            if (count($dumps) >= static::getConfig('maxStorageDumpCount')) {
                $file = array_shift($dumps);
                $file = end($file);
                if (file_exists($file)) {
                    if (is_writable($file)) {
                        unlink($file);
                    } else {
                        throw new FileIsNotWriteable($file);
                    }
                } else {
                    throw new NotFoundFile($file);
                }

                Storage::setSection('dumps', $dumps);
            }
        }

        /**
         * Добавление последней версии дампа
         */
        Storage::setById('dumps', $timeAdd,
            [
                $hashCommit => $archiveFile
            ]
        );

        parent::upgrade();
    }

    /**
     * Процедура отката к предыдущему деплою
     *
     * @throws LoggedException
     */
    public function downgrade()
    {
        /**
         * откат миграций
         */
        $this->downgradeMigrations();

        /**
         * откат файлов проекта
         */
        $this->downgradeDeploy();

        parent::downgrade();
    }

    /**
     * Процедура отката базы данных до предыдущего состояния
     */
    public function downgradeDatabase()
    {
        $deploys = Storage::get("deploys");

        if (empty($deploys)) {
            throw new LoggedException('Deploy list is empty');
        }

        $currentDeploy = array_pop($deploys);
        Directory::setCurrentDirectory($this->deployArchiveDirectory);

        /**
         * распаковка и загрузка дампа базы данных
         */
        $dbSettingsFile = $this->deployDirectory . "/config/connect_db.php";
        $dbSettings = file_exists($dbSettingsFile) ? include $dbSettingsFile : [];

        if (empty($dbSettings)) {
            throw new BadParameters('Database settings failed');
        }

        $output = Shell::command(sprintf('gunzip < %s | mysql -u %s -p%s -h%s %s',
            $currentDeploy['archiveFile'],
            $dbSettings['user'],
            $dbSettings['pas'],
            $dbSettings['host'],
            $dbSettings['db_name']
        ));

        if ($output[1]) {
            throw new BadParameters(sprintf('Database import failed: %s', $output[0]));
        }

        return true;
    }

    /**
     * обновление миграции проекта
     */
    public function upgradeMigrations()
    {

        $result = null;
        /**
         * Алгоритм:
         *  1 - проверить количество миграций проекта
         *  2 - вызвать migrate
         *  3 - проверить количество миграций проекта если есть расхождения, записать ключ в миграции (хэш => id)
         *  4 - проверить что id нет в миграциях
         */
        $startCount = Migration::getCountRecords();

        Migration::getInstance()->upgrade();

        $endCount = Migration::getCountRecords();

        if ($endCount > $startCount) {
            $migrationLastId = Migration::getLastId();
            if ($migrationLastId && !Migration::has($migrationLastId)) {

                /**
                 * Получение Хэша последнего комита текущей задеплоеной версии
                 */
                $hashCommit = Git::getCurrentCommit($this->deployGitDirectory . DIRECTORY_SEPARATOR . Git::getProjectName($this->gitRepository));
                Migration::setLastVersion($hashCommit, $migrationLastId);
            }
        }
    }

    /**
     * откат миграции проекта
     */
    public function downgradeMigrations()
    {
        /**
         * Настройки базы данных
         */
        $projectDirectory = $this->deployGitDirectory . DIRECTORY_SEPARATOR . Git::getProjectName($this->gitRepository);
        $dbSettingsFile = $projectDirectory . "/config/connect_db.php";
        $dbSettings = file_exists($dbSettingsFile) ? include $dbSettingsFile : [];

        if (empty($dbSettings)) {
            throw new BadParameters('Database settings failed');
        }

        /**
         * Подключение к БД для миграции
         */
        Database::connect($dbSettings['host'], $dbSettings['user'], $dbSettings['pas'], $dbSettings['db_name']);

        /**
         * Получение Хэша последнего комита текущей задеплоеной версии
         */
        $hashCommit = Git::getCurrentCommit($projectDirectory);
        Directory::setCurrentDirectory($projectDirectory . '/database');

        /**
         * Автоматический откат к предыдущей миграции, если к текущему комиту есть связанная миграция
         */
        if ($current = Migration::getVersion($hashCommit)) {

            $previous = Migration::getPrevious($hashCommit);
            $migrations = Storage::getSection('migrations');

            if ($previous) {
                $previousHash = end(array_keys($previous));
                $migrateStepCount = Migration::getStepCount($previousHash);

                /**
                 * количество необходимых откатов теоретически может быть больше 1
                 */
                for ($migratePosition = 0; $migratePosition < $migrateStepCount; $migratePosition++) {
                    Migration::getInstance()->downgrade();
                    array_shift($migrations);
                }
            }

            Storage::setSection('migrations', $migrations);
        }
    }

    /**
     * откат деплоя (файловые операции)
     */
    public function downgradeDeploy()
    {
        $deploys = Storage::getSection("deploys");

        if (empty($deploys)) {
            throw new LoggedException('Deploy list is empty');
        }

        $deployCount = count($deploys);

        if ($deployCount > 1) {

            $currentDeploy = array_pop($deploys);
            $currentDeployName = end($currentDeploy);

            $prevDeploy = end($deploys);
            $prevDeployName = end($prevDeploy);

            /**
             * Убираем симлинк на текущую версию
             */
            if (Symlink::isSymlink($this->deployDirectory)) {
                Symlink::removeSymlink($this->deployDirectory);
            }

            /**
             * Добавление ссылки на предыдущий деплой
             */
            $sourcePath = $this->deploySourceDirectory . DIRECTORY_SEPARATOR . $prevDeployName;

            if (!Symlink::addSymlink($sourcePath, $this->deployDirectory)) {
                throw new BadParameters('Not create symlink');
            }

            /**
             * сохранение состояния списка деплоев
             */
            Storage::setSection("deploys", $deploys);

            return true;
        }

        return false;
    }

    /**
     * откат дампа БД к предыдущей версии
     */
    public function downgradeDump()
    {
        $dumps = Storage::getSection("dumps");

        if (empty($dumps)) {
            throw new LoggedException('Deploy list is empty');
        }

        if (count($dumps) >= 2) {
            $currentDump = array_pop($dumps);
            $prevDump = end($dumps);
            $prevDumpFile = end($prevDump);


            $dbSettingsFile = $this->deployDirectory . "/config/connect_db.php";
            $dbSettings = file_exists($dbSettingsFile) ? include $dbSettingsFile : [];

            if (empty($dbSettings) && !isset($dbSettings['db_name'])) {
                throw new BadParameters('Database settings failed');
            }

            /**
             * Удаление базы данных
             */
            $dropDatabaseSql = 'DROP DATABASE IF EXISTS ' . $dbSettings['db_name'];
            MysqlDump::command($dropDatabaseSql, $dbSettings['user'], $dbSettings['pas'], $dbSettings['host']);

            /**
             * Добавление базы данных
             */
            $dropDatabaseSql = 'CREATE DATABASE ' . $dbSettings['db_name'];
            MysqlDump::command($dropDatabaseSql, $dbSettings['user'], $dbSettings['pas'], $dbSettings['host']);

            /**
             * загрузка архива с дампом бд
             */
            MysqlDump::import($prevDumpFile, $dbSettings['user'], $dbSettings['pas'], $dbSettings['host'], $dbSettings['db_name']);

            /**
             * сохранение состояния списка дампов
             */
            Storage::setSection('dumps', $dumps);
        }
    }


    /**
     * @future Процедура переключения к определенному деплою
     *
     * @status in the design state
     * @throws LoggedException
     */
    public function setgrade(string $commitHash = null)
    {
        $deploys = Storage::get("deploys");
    }

    /**
     * запуск тестов проекта
     */
    public function executeTests()
    {
        if (static::getConfig('isTestsExecute')) {
            Logger::Log("Running tests!");
            $result = $this->afterTests(static::getConfig('isSilentTestFail', true));
            if ($result) {
                Logger::Log("Result tests: OK");
            } else {
                $data = $this->getTestsData();
                Logger::Log(sprintf("Result tests: FAIL\n%s\n-------------------------\n", $data));
            }
        }

        Logger::Log(sprintf("Deploy::upgrade(%s)", $this->procname));

        return $data;

    }

    /**
     * Запуск создания директорий для развертывания проекта
     *
     * @return array|void
     */
    public function installProject()
    {
        $config = self::getConfig();

        $directories = [
            isset($config['dumpDirectory']) ? $config['dumpDirectory'] : '',
            isset($config['deployStaticDirectory']) ? $config['deployStaticDirectory'] : '',
            isset($config['deployArchiveDirectory']) ? $config['deployArchiveDirectory'] : '',
            isset($config['deploySourceDirectory']) ? $config['deploySourceDirectory'] : '',
            isset($config['deployGitDirectory']) ? $config['deployGitDirectory'] : '',
            isset($config['deployDirectory']) ? $config['deployDirectory'] : ''
        ];

        foreach ($directories as $directory) {
            if (!empty($directory)) {
                Directory::create($directory);
            }
        }

        return true;
    }
}