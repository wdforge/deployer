<?php

/**
 * настройки развертывания проекта
 */
return [
    /**
     * параметр устанавливаемый из скрипта запуска (имя скрипта)
     */
    'procname' => 'vitrina-tizerov',
    'logfile' => __DIR__.'/log/deploy.log',
    'adminEmail' => 'admin@test.ru',
    'isLogging' => true,
    'storageFile' => __DIR__ . '/storage/db.php',
    'dumpAfterDeploy' => '',
    /**
     * перехват ошибок
     */
    'isHookError' => false,
    /**
     * проверка на запуск копии процесса
     */
    'singleProcess' => true,

    /**
     * Настройки git для развертывания проекта
     */
    'gitRepository' => 'git@gitlab.com:devtizerteam/vitrina-tizerov.git',

    /**
     * фиксированное состояние
     */
    'gitCommit' => 'devone',

    /**
     * рабочая ветка проекта
     */
    'gitBranch' => 'devone',

    /**
     * флаг устанавливаемый из параметров запуска
     */
    'dispatch_mode' => 'deploy',

    /**
     * класс приложения для сценария выполнения деплоя
     */
    'deployClass' => \Vitrina\Deploy::class,

    /**
     * то куда будет деплоится рабочая версия проекта
     */
    'deployDirectory' => '/var/www/project',

    /**
     * максимальное количество хранения последних версий проекта
     */
    'maxDeployCount' => 4,

    /**
     * где будет находится последния копия с гитом и vendor
     */
    'deployGitDirectory' => '/home/deploy/vitrina/git',

    /**
     * где будут храниться последние несколько версий проекта
     */
    'deploySourceDirectory' => '/home/deploy/vitrina/source',

    /**
     * куда будут скидываться архивы с последними копиями проекта
     */
    'deployArchiveDirectory' => '/home/deploy/vitrina/archive',

    /**
     * директория для хранения статики проекта
     */
    'deployStaticDirectory' => '/home/deploy/vitrina/static',

    /**
     * описания контроллеров API
     */
    'controllers' => [
        'vitrina' => Vitrina\Controller\VitrinaController::class
    ],

    /**
     * описания роутинга API
     */
    'routes' => [
        'vitrina' => [
            'upgrade',
            'downgrade',
            'downgradeDeploy',
            'downgradeDump',
            'executeTests',
            'upgradeMigrations',
            'downgradeMigrations',
        ],
    ],

    /**
     * настройки сервера задач
     */
    'gearmanServer' => '127.0.0.1',
    'gearmanPort' => '4730',

    /**
     * директория для хранения дампов бд проекта
     */
    'dumpDirectory' => '/home/deploy/vitrina/archive',
    'maxStorageDumpCount' => 4,

    'isSilentDumpFail' => false,
    'isInterruptSqlFail' => false,
    'isTestsExecute' => true,
    'isSilentTestFail' => true,

    'beforeTestsDirectory' => __DIR__.'/tests/before',
    'afterTestsDirectory' => __DIR__.'/tests',

];
